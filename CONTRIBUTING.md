# Contributing

Contributions are welcome! However, we cannot accept any merge requests until
January of 2017. If you have questions or comments about the project, feel free
to add an issue, or email tommydean_sd(at)yahoo(dot)com.