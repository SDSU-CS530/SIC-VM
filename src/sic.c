#include "../include/sic.h"
#include "../include/instructions.h"
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>

unsigned char *memory;

/* Registers */
int32_t A;
uint32_t X;
uint32_t L;
uint32_t PC;
uint32_t SW;

/* IO Devices */
FILE *DEVF1, // input device 1
     *DEVF2, // input device 2
     *DEVF3, // input device 3
     *DEV04, // output device 1
     *DEV05, // output device 2
     *DEV06; // output device 3

unsigned char
read_device(unsigned char dev)
{
    int val;
    switch (dev) {
        case 0xF1:
            val = fgetc(DEVF1);
            break;
        case 0xF2:
            val = fgetc(DEVF2);
            break;
        case 0xF3:
            val = fgetc(DEVF3);
            break;
        default:
            val = EOF;
    }

    if (EOF == val) {
        return 0x00;
    }
    return val;
}

void
write_device(unsigned char dev, char character)
{
    switch (dev) {
        case 0x04:
            fputc(character, DEV04);
            break;
        case 0x05:
            fputc(character, DEV05);
            break;
        case 0x06:
            fputc(character, DEV06);
            break;
    }
}

int
run()
{
    // TODO: Check for overflow on arithmatic operations
    int32_t tmp;
    unsigned char opcode;
    unsigned int address;

    /* Instruction table used for direct threading */
    static const void *instr_tab[] = {                              /* opcodes */
        &&LBL_LDA,          &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x00 - 0x03 */
        &&LBL_LDX,          &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x04 - 0x07 */
        &&LBL_LDL,          &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x08 - 0x0b */
        &&LBL_STA,          &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x0c - 0x0f */
        &&LBL_STX,          &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x10 - 0x13 */
        &&LBL_STL,          &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x14 - 0x17 */
        &&LBL_ADD,          &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x18 - 0x1b */
        &&LBL_SUB,          &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x1c - 0x1f */
        &&LBL_MUL,          &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x20 - 0x23 */
        &&LBL_DIV,          &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x24 - 0x27 */
        &&LBL_COMP,         &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x28 - 0x2b */
        &&LBL_TIX,          &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x2c - 0x2f */
        &&LBL_JEQ,          &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x30 - 0x33 */
        &&LBL_JGT,          &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x34 - 0x37 */
        &&LBL_JLT,          &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x38 - 0x3b */
        &&LBL_J,            &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x3c - 0x3f */
        &&LBL_AND,          &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x40 - 0x43 */
        &&LBL_OR,           &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x44 - 0x47 */
        &&LBL_JSUB,         &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x48 - 0x4b */
        &&LBL_RSUB,         &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x4c - 0x4f */
        &&LBL_LDCH,         &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x50 - 0x53 */
        &&LBL_STCH,         &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x54 - 0x57 */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x58 - 0x5b */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x5c - 0x5f */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x60 - 0x63 */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x64 - 0x67 */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x68 - 0x6b */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x6c - 0x6f */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x70 - 0x73 */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x74 - 0x77 */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x78 - 0x7b */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x7c - 0x7f */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x80 - 0x83 */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x84 - 0x87 */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x88 - 0x8b */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x8c - 0x8f */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x90 - 0x93 */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x94 - 0x97 */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x98 - 0x9b */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0x9c - 0x9f */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0xa0 - 0xa3 */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0xa4 - 0xa7 */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0xa8 - 0xab */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0xac - 0xaf */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0xb0 - 0xb3 */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0xb4 - 0xb7 */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0xb8 - 0xbb */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0xbc - 0xbf */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0xc0 - 0xc3 */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0xc4 - 0xc7 */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0xc8 - 0xcb */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0xcc - 0xcf */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0xd0 - 0xd3 */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0xd4 - 0xd7 */
        &&LBL_RD,           &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0xd8 - 0xdb */
        &&LBL_WD,           &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0xdc - 0xdf */
        &&LBL_TD,           &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0xe0 - 0xe3 */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0xe4 - 0xe7 */
        &&LBL_STSW,         &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0xe8 - 0xeb */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0xec - 0xef */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0xf0 - 0xf3 */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0xf4 - 0xf7 */
        &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, /* 0xf8 - 0xfb */
        &&LBL_HALT,         &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED, &&LBL_UNRECOGNIZED  /* 0xfc - 0xff */
    };

    NEXT


LBL_ADD:
    /* A <- (A) + (m .. m+2) */
    memcpy(&tmp, memory + address, WORD_SIZE);
    tmp = __builtin_bswap32(tmp);
    tmp >>= 8;
    A += tmp;
    NEXT
LBL_AND:
    /* A <- (A) & (m .. m+2) */
    memcpy(&tmp, memory + address, WORD_SIZE);
    tmp = __builtin_bswap32(tmp);
    tmp >>= 8;
    A &= tmp;
    NEXT
LBL_COMP: 
    /* A : (m .. m+2) */
    SW &= 0xFFFFF8; // Clear Status Word, TODO: Use macro constant for mask
    memcpy(&tmp, memory + address, WORD_SIZE);
    tmp = __builtin_bswap32(tmp);
    tmp >>= 8;
    if (A < tmp) {
        SW |= LT;
    } else if (A > tmp) {
        SW |= GT;
    } else {
        SW |= EQ;
    }
    NEXT
LBL_DIV:
    /* A = (A) / (m .. m+2) */
    memcpy(&tmp, memory + address, WORD_SIZE);
    tmp = __builtin_bswap32(tmp);
    tmp >>= 8;
    if (tmp == 0) {
        return VM_ERROR_DIV_BY_ZERO;
    }
    A /= tmp;
    NEXT
LBL_J:
    /* PC <- m */
    PC = address;
    NEXT
LBL_JEQ:
    /* PC <- m if CC set to EQ */
    if (SW & EQ) {
        PC = address;
    }
    NEXT
LBL_JGT:
    /* PC <- m if CC set to GT */
    if (SW & GT) {
        PC = address;
    }
    NEXT
LBL_JLT:
    /* PC <- m if CC set to LT */
    if (SW & LT) {
        PC = address;
    }
    NEXT
LBL_JSUB:
    /* L <- (PC); PC <- m */
    L = PC;
    PC = address;
    NEXT
LBL_LDA:
    /* A <- (m .. m+2) */
    memcpy(&A, memory + address, WORD_SIZE);
    A = __builtin_bswap32(A);
    A >>= 8;
    NEXT
LBL_LDCH:
    /* A [rightmost byte] <- (m) */
    *(char *)&A = memory[address];
    NEXT
LBL_LDL:
    /* L <- (m .. m+2) */
    memcpy(&L, memory + address, WORD_SIZE);
    L = __builtin_bswap32(L);
    L >>= 8;
    NEXT
LBL_LDX:
    /* X <- (m .. m+2) */
    memcpy(&X, memory + address, WORD_SIZE);
    X = __builtin_bswap32(X);
    X >>= 8;
    NEXT
LBL_MUL:
    /* A = (A) * (m .. m+2) */
    memcpy(&tmp, memory + address, WORD_SIZE);
    tmp = __builtin_bswap32(tmp);
    tmp >>= 8;
    A *= tmp;
    NEXT
LBL_OR:
    /* A <- (A) | (m .. m+2) */
    memcpy(&tmp, memory + address, WORD_SIZE);
    tmp = __builtin_bswap32(tmp);
    tmp >>= 8;
    A |= tmp;
    NEXT
LBL_RD:
    /* A [rightmost byte] <- device[(m)] */
    *(char *)&A = read_device(memory[address]);
    NEXT
LBL_RSUB:
    /* PC <- (L) */
    PC = L;
    NEXT
LBL_STA:
    /* m .. m+2 <- (A) */
    memory[address] = *((char *)&A + 2);
    memory[address + 1] = *((char *)&A + 1);
    memory[address + 2] = *(char *)&A;
    NEXT
LBL_STCH:
    /* m <- (A [rightmost byte]) */
    memory[address] = *(char *)&A;
    NEXT
LBL_STL:
    /* m .. m+2 <- (L) */
    memory[address] = *((char *)&L + 2);
    memory[address + 1] = *((char *)&L + 1);
    memory[address + 2] = *(char *)&L;
    NEXT
LBL_STSW:
    /* m .. m+2 <- (SW) */
    memory[address] = *((char *)&SW + 2);
    memory[address + 1] = *((char *)&SW + 1);
    memory[address + 2] = *(char *)&SW;
    NEXT
LBL_STX:
    /* m .. m+2 <- (X) */
    memory[address] = *((char *)&X + 2);
    memory[address + 1] = *((char *)&X + 1);
    memory[address + 2] = *(char *)&X;
    NEXT
LBL_SUB:
    /* A <- (A) - (m .. m+2) */
    memcpy(&tmp, memory + address, WORD_SIZE);
    tmp = __builtin_bswap32(tmp);
    tmp >>= 8;
    A -= tmp;
    NEXT
LBL_TD:
    /* CC <- LT if device[(m)] is ready;
     * else CC <- EQ (device is not ready) */
    SW &= 0xFFFFF8; // Clear Status Word, TODO: Use macro constant for mask
    SW |= LT; // The device is always ready ... TODO: Should this this simulate a random delay?
    NEXT
LBL_TIX:
    /* X <- (X) + 1; (X) : (m .. m+2) */
    SW &= 0xFFFFF8; // Clear Status Word, TODO: Use macro constant for mask
    memcpy(&tmp, memory + address, WORD_SIZE);
    tmp = __builtin_bswap32(tmp);
    tmp >>= 8;
    X++;
    if (X < tmp) {
        SW |= LT;
    } else if (X > tmp) {
        SW |= GT;
    } else {
        SW |= EQ;
    }
    NEXT
LBL_WD:
    /* device[(m)] <- (A [rightmost byte]) */
    write_device(memory[address], *(char *)&A);
    NEXT
LBL_HALT:
    /* Stop execution */
    return VM_SUCCESS;
LBL_UNRECOGNIZED:
    return VM_ERROR_UNRECOGNIZED_OPCODE;
}

    int
init_io(void)
{
    if ((DEVF1 = fopen("DEVF1", "r")) == NULL) {
        perror("sic: init_io: DEVF1");
        return VM_ERROR_INIT_IO;
    }
    if ((DEVF2 = fopen("DEVF2", "r")) == NULL) {
        perror("sic: init_io: DEVF2");
        return VM_ERROR_INIT_IO;
    }
    if ((DEVF3 = fopen("DEVF3", "r")) == NULL) {
        perror("sic: init_io: DEVF3");
        return VM_ERROR_INIT_IO;
    }
    if ((DEV04 = fopen("DEV04", "w")) == NULL) {
        perror("sic: init_io: DEV04");
        return VM_ERROR_INIT_IO;
    }
    if ((DEV05 = fopen("DEV05", "w")) == NULL) {
        perror("sic: init_io: DEV05");
        return VM_ERROR_INIT_IO;
    }
    if ((DEV06 = fopen("DEV06", "w")) == NULL) {
        perror("sic: init_io: DEV06");
        return VM_ERROR_INIT_IO;
    }

    return VM_SUCCESS;
}

    int
start_vm(struct vm_options options)
{
    /* Validate input parameters */
    if (options.program_length + options.program_offset > MEM_SIZE) {
        return VM_ERROR_PROGRAM_LENGTH_OVERFLOW;
    }
    if (options.program_length == 0) {
        return VM_ERROR_ZERO_PROGRAM_LENGTH;
    }

    /* Set up main memory */
    if (NULL == (memory = malloc(MEM_SIZE))) {
        return VM_ERROR_ENOMEM;
    }
    memcpy(memory + options.program_offset, options.program, options.program_length);

    /* Init PC according to parameters */
    PC = options.program_offset;

    if (init_io() != VM_SUCCESS) {
        return VM_ERROR_INIT_IO;
    }

    return run();
}

    int
stop_vm(void)
{
    // TODO: Stop execution of vm
    if (NULL != memory) {
        free(memory);
    }

    fclose(DEVF1);
    fclose(DEVF2);
    fclose(DEVF3);
    fclose(DEV04);
    fclose(DEV05);
    fclose(DEV06);

    return VM_SUCCESS;
}

