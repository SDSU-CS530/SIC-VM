# SIC Virtual Machine

The SIC, or **S**implified **I**nstructional **C**omputer, architecture is a
theoretical system developed by Leland Beck in order to teach systems
programming concepts in a clear and straight forward way. This virtual machine
is a simulation of the aforementioned theoretical system.

## Motivation

The [ current implementations ]( https://en.wikipedia.org/wiki/Simplified_Instructional_Computer#Emulating_the_SIC_System )
of SIC simulators are slow and generally out of date. Therefore, SIC-VM will use 
many [ optimizations ]( #optimizations ) to improve the virtualization of the SIC 
architecture. This implementation also serves as a learning platform for systems 
software students to better understand the theoretical architecture that they are 
studying by actually seeing the results that the CPU might produce. 

## Optimizations

In order to increase the efficiency of the virtual machine, SIC-VM uses many
software virtualization techniques described chapters 3 and 4 of [Yunhe Shi's 
Virtual Machine Showdown: Stack verses Registers]( https://www.cs.tcd.ie/publications/tech-reports/reports.07/TCD-CS-2007-49.pdf ).

### Optimization techniques
* [ Direct threaded code for opcode decoding and dispatch ]( http://groups.csail.mit.edu/pag/OLD/parg/piumarta98optimizing.pdf )
* [ Indirect branch prediction ]( https://www.cs.tcd.ie/David.Gregg/papers/toplas05.pdf )
* [ Trace caching ]( https://www.eecs.umich.edu/techreports/cse/99/CSE-TR-394-99.pdf )

### Optimization implementation

Direct threaded code is implemented using GCC's [ labels as values ]( https://gcc.gnu.org/onlinedocs/gcc/Labels-as-Values.html )
where the opcode table is an array of ```void *``` each pointing to the location 
in memory that contains the handler for the opcode. After one instruction is 
run, the next is fetched in place and found in the opcode table. 

#### Sample program using GCC's [ labels as values ]( https://gcc.gnu.org/onlinedocs/gcc/Labels-as-Values.html )

```c
#include <stdio.h>

int main(int argc, char *argv[])
{
    int A = 0xFFFFFF, vPC = 0;
    const void *optab[] = { &&INC, &&PRTA, &&LD100, &&HALT };
    const int prog[] = { 
        0x2, // LD100
        0x0, // INC
        0x1, // PRTA
        0x3  // HALT
    };

    goto *(optab[prog[vPC++]]);

    /* Instruction implementations */
INC:
    A += 1;
    goto *(optab[prog[vPC++]]);

PRTA:
    printf("A: %d\n", A);
    goto *(optab[prog[vPC++]]);

LD100:
    A = 100;
    goto *(optab[prog[vPC++]]);

HALT:
    return 0;
}

```

* TODO: Indirect branch prediction
* TODO: Trace caching

## Contributors
* Thomas Dean
* Michael Green
* Brexton Popejoy

