#ifndef _SIC_H
#define _SIC_H

#include <stddef.h>

#define VM_SUCCESS                          0
#define VM_ERROR_PROGRAM_LENGTH_OVERFLOW    1
#define VM_ERROR_ZERO_PROGRAM_LENGTH        2
#define VM_ERROR_ENOMEM                     3
#define VM_ERROR_UNRECOGNIZED_OPCODE        4
#define VM_ERROR_DIV_BY_ZERO                5
#define VM_ERROR_INIT_IO                    6
#define MEM_SIZE                            32768
#define WORD_SIZE                           3
#define NEXT opcode = memory[PC];\
    address = memory[PC + 1] & 0x7F;\
    address <<= 8;\
    address |= memory[PC + 2];\
    if (memory[PC + 1] & 0x80) {\
        address += X;\
    }\
    PC += 3;\
    goto *instr_tab[opcode];

struct vm_options {
    unsigned char * program; /* Pointer to the start of the program */
    size_t program_length; /* Length of the program in bytes, must be a multiple of 3 */
    int program_offset; /* Where to load the program into memory */
};

int start_vm(struct vm_options options);
int stop_vm(void);

#endif // #ifdef _SIC_H
