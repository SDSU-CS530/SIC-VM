#ifndef _INSTRUCTIONS_H
#define _INSTRUCTIONS_H

/*      INSTR   OPCODE */
#define ADD     0x18
#define AND     0x40
#define COMP    0x28
#define DIV     0x24
#define J       0x3C
#define JEQ     0x30
#define JGT     0x34
#define JLT     0x38
#define JSUB    0x48
#define LDA     0x00
#define LDCH    0x50
#define LDL     0x08
#define LDX     0x04
#define MUL     0x20
#define OR      0x44
#define RD      0xD8
#define RSUB    0x4C
#define STA     0x0C
#define STCH    0x54
#define STL     0x14
#define STSW    0xE8
#define STX     0x10
#define SUB     0x1C
#define TD      0xE0
#define TIX     0x2C
#define WD      0xDC

/* Ends CPU execution */
#define HALT    0xFC

/* Condition codes */
#define LT 0x01
#define EQ 0x02
#define GT 0x04

#endif // #ifndef _INSTRUCTIONS_H

